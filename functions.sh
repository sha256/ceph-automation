#!/bin/bash
homepath="$(cd "$(dirname "$0")" && pwd)"
source $homepath/settings.sh
webhookfunction () {
if [[ $webhookurl == *"webhook.site"* ]]; then
curl -H "Content-Type: application/json" -X POST -d $"$1" $webhookurl
elif [[ $webhookurl == *"slack.com"* ]]; then
curl -X POST --data-urlencode "payload={\"channel\": \"#$slackchannel\", \"username\": \"Ceph Automation\", \"text\": \"$1\", \"icon_emoji\": \":ghost:\"}" $webhookurl
else
echo "Use Slack or Webhooks.site for webhook url"
exit 1
fi
echo 'Webhook Sent'
}

