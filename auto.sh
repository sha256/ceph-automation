#!/bin/bash
# V1.3
homepath="$(cd "$(dirname "$0")" && pwd)"
source $homepath/functions.sh
cephhealth=$(sudo ceph health detail)
webhookfunction "$cephhealth"


osddf=$(sudo ceph osd df|sort -nk17|head;sudo ceph osd df|sort -nk17|tail)
webhookfunction "\`\`\`$osddf\`\`\`"
