#!/bin/bash 
envpath=$(env | grep PATH)
homepath="$(cd "$(dirname "$0")" && pwd)"
chmod +x $homepath/*.sh
sed -i "s~PATH=~$envpath~g" $homepath/settings.sh
crontab -l > cronfile
echo "00 6 * * 1-5 $homepath/auto.sh" >> cronfile
echo "00 13 * * 1-5 $homepath/auto.sh" >> cronfile
crontab cronfile
rm cronfile
rm -- "$0"